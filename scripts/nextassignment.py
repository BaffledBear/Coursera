#!/usr/bin/env python
"""
title           :nextassignment.py
description     :This script creates a new directory with nessesary stuff
author          :BaffledBear (Peter Rauhut)
date            :20161008
version         :0.1
usage           :python3 nextassignment.py
notes           :CTRL-C to cancel
python version  :3.5.2
"""
import argparse
import os
import sys
from datetime import date

if (sys.version_info.major < 3):
    sys.exit("Requires Python 3 or greater...")

test_generator_header = {
    'title': 'test_generator.py',
    'description': 'Automated stress tests for project',
    'author': 'BaffledBear (Peter Rauhut)',
    'date': date.today().isoformat().replace('-', ''),
    'version': '0.1',
    'usage': 'python3 test_generator.py',
    'notes': 'CTRL-C to cancel',
    'python version': '3.5.2',
    'other': """import time


def test_generator():
    \"\"\"
    Generates random tests
    \"\"\"
    test = "I'm not filled out"
    return test


if __name__ == \"__main__\":
    f = open('Error.log', 'w')
    while(True):
        break
    start = time.time()
    print(test_generator())
    end = time.time()
    print("Runtime: {} seconds".format(end - start))
"""
}


project_file_header = {
    'title': '',
    'description': 'This script solves the problem',
    'author': 'BaffledBear (Peter Rauhut)',
    'date': date.today().isoformat().replace('-', ''),
    'version': '0.1',
    'usage': 'python3 ',
    'notes': 'N/A',
    'python version': '3.5.2'
}


def format_string_to_dict(name):
    return {
        'name': name,
        'title': '_'.join(name.title().split(' ')),
        'lower': '_'.join(name.lower().split(' '))
    }


def parse_coursera_directory():
    current_directory = os.getcwd()
    if ('Coursera' in current_directory):
        index = current_directory.index('Coursera') + len('Coursera')
        return current_directory[0:index + 1] + '/'
    else:
        sys.exit("Must be in Coursera directory or a subdirectory.")


def get_highest_assignment(arr, assignment):
    highest = 0
    for i in arr:
        index = int(i[0:3])
        if i[4:] == assignment:
            sys.exit("Assignment already exists...")
        if index > highest:
            highest = index
    target = highest + 1
    high_str = target if target > 99 else (
        '0{}'.format(target) if target > 9 else '00{}'.format(target))
    return high_str


def write_to_file(filename, file_dict):
    f = open(filename, 'w')
    f.write('"""\n')
    f.write('title           :{}\n'.format(file_dict['title']))
    f.write('description     :{}\n'.format(file_dict['description']))
    f.write('author          :{}\n'.format(file_dict['author']))
    f.write('date            :{}\n'.format(file_dict['date']))
    f.write('version         :{}\n'.format(file_dict['version']))
    f.write('usage           :{}\n'.format(file_dict['usage']))
    f.write('notes           :{}\n'.format(file_dict['notes']))
    f.write('python version  :{}\n'.format(file_dict['python version']))
    f.write('"""\n')
    f.write(file_dict['other'])


if __name__ == '__main__':
    PYTHON_VERSION = '{}.{}.{}'.format(
        sys.version_info.major,
        sys.version_info.minor,
        sys.version_info.micro
    )
    COURSERA_DIR = parse_coursera_directory()
    parse_desc = 'Create directory and dependencies for Coursera assignments.'
    parser = argparse.ArgumentParser(
        description=parse_desc
    )
    parser.add_argument('course',
                        help='The name of the course on Coursera.')
    parser.add_argument('assignment',
                        help='The name of the assignment on Coursera.')
    args = parser.parse_args()
    assignment = format_string_to_dict(args.assignment)
    course = format_string_to_dict(args.course)
    course_dir = COURSERA_DIR + course['title'] + '/'
    if not os.path.exists(course_dir):
        os.makedirs(course_dir)
    assignments_list = os.listdir(course_dir)
    next_assignment = get_highest_assignment(
        assignments_list,
        assignment['title']
    )
    assignment_dir = '{}{}-{}/'.format(
        course_dir,
        next_assignment,
        assignment['title']
    )
    if not os.path.exists(assignment_dir):
        os.makedirs(assignment_dir)
    project_file_header['title'] = '{}.py'.format(assignment['lower'])
    project_file_header['usage'] = (
        project_file_header['usage'] +
        project_file_header['title']
    )
    project_file_header['other'] = "\n\ndef {}():\n    pass\n".format(
        assignment['lower']
    )
    write_to_file('{}{}.py'.format(
        assignment_dir,
        assignment['lower']
    ), project_file_header)
    write_to_file('{}{}'.format(
        assignment_dir,
        test_generator_header['title'],
    ), test_generator_header)
