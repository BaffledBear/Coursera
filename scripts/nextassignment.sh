#!/usr/bin/env bash

# title           :nextassignment.sh
# description     :This script will create new directories for assignments
# author          :BaffledBear (Peter Rauhut)
# date            :20161008
# version         :0.1
# usage           :.scripts/nextassignment.sh {course} {assignment}
# notes           :course and assignment will be directory names
# notes           :Run from Coursera Directory
# bash version    :4.3.46(1)-release (x86_64-pc-linux-gnu)

# This script takes 1 argument, the name of the project, and creates a
# directory for it. It will place a template test_generator.py file in
# the directory. Always run this from the Coursera directory
COURSERADIR=`pwd`
COURSE=$1
PROJECT=$2
NEWDIR=$COURSERADIR/$COURSE/$PROJECT
SCRIPTDIR=$COURSERADIR/scripts/
read -n1 -r -p "Creating $NEWDIR. CTRL-C to cancel.." key
mkdir $NEWDIR
cp $SCRIPTDIR/test_generator_template.py $NEWDIR/test_generator.py
