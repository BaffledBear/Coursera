# This file will run infinitely until stopped manually.
# Use ctrl-c to stop this test.
# This is a stress test for the {project name} problem.
# It generates a random test and executes it against the fast and slow
# versions of the {method name} method.
import time


def test_generator():
    """
    Generates random tests
    """
    test = "I'm not filled out"
    return test


if __name__ == "__main__":
    f = open('Error.log', 'w')
    while(True):
        break
    start = time.time()
    print(test_generator())
    end = time.time()
    print("Runtime: {} seconds".format(end - start))
