"""
title           :max_pairwise_product_slow.py
description     :This script solves the problem slowly
author          :BaffledBear (Peter Rauhut)
date            :20161008
version         :0.1
usage           :python3 max_pairwise_product_slow.py
notes           :input one int followed by space delimited list of ints
python version  :3.5.2
"""

# This is a slow but accurate solution to the problem for stress testing
# purposes.


def max_pairwise_product(arr):
    result = 0
    for i in range(0, len(arr)):
        for j in range(i + 1, len(arr)):
            if arr[i] * arr[j] > result:
                result = arr[i] * arr[j]
    return result


if __name__ == "__main__":
    n = int(input())
    if (n < 2):
        print("Insufficient data points to provide solution.")
    else:
        a = [int(x) for x in input().split()]
        assert(len(a) == n)
        print(max_pairwise_product(a))
