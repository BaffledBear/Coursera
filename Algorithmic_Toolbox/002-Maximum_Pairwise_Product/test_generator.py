"""
title           :test_generator.py
description     :This script stress tests max_pairwise_product.py
author          :BaffledBear (Peter Rauhut)
date            :20161008
version         :0.1
usage           :python3 test_generator.py
notes           :CTRL-C to cancel, Modify n in test_generator() if needed
python version  :3.5.2
"""


# This file will run infinitely until stopped manually.
# Use ctrl-c to stop this test.
# This is a stress test for the max_pairwise_product problem.
# It generates a random test and executes it against the fast and slow
# versions of the max_pairwise_product method.
import max_pairwise_product
import max_pairwise_product_slow
import random
import time


def test_generator():
    """
    Generates random tests
    """
    n = random.randint(2, 12)  # Use this line to generate small tests
    # n = 200000  # Use this line to generate large tests. This takes a while.
    test = {
        'n': n,
        'arr': []
    }
    for i in range(0, n):
        test['arr'].append(random.randint(2, 100000))
    return test


if __name__ == "__main__":
    f = open('Error.log', 'w')
    while(True):
        test = test_generator()
        if (test['n'] < 10001):
            result1 = max_pairwise_product.max_pairwise_product(test['arr'])
            result2 = max_pairwise_product_slow.max_pairwise_product(
                test['arr']
            )
            if (result1 == result2):
                print("Test OK")
            else:
                print("Test Failed: {}, {}, {}".format(
                    result1,
                    result2,
                    test
                ))
                f.write("Test Failed: {}, {}, {}".format(
                    result1,
                    result2,
                    test
                ))
        else:
            print("Timer")
            start = time.time()
            max_pairwise_product.max_pairwise_product(
                test['arr']
            )
            end = time.time()
            print(end - start)
