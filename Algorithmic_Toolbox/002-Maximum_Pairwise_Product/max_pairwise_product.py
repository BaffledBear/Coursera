"""
title           :max_pairwise_product.py
description     :This script solves the problem quickly
author          :BaffledBear (Peter Rauhut)
date            :20161008
version         :0.1
usage           :python3 max_pairwise_product.py
notes           :input one int followed by space delimited list of ints
python version  :3.5.2
"""


# This is a faster solution to the problem described in Directions.txt
def max_pairwise_product(arr):
    highest = 0
    second = 0
    for num in arr:
        if num > highest:
            second = highest
            highest = num
        elif num > second:
            second = num
    return highest * second


if __name__ == "__main__":
    n = int(input())
    if (n < 2):
        print("Insufficient data points to provide solution.")
    else:
        a = [int(x) for x in input().split()]
        assert(len(a) == n)
        print(max_pairwise_product(a))
