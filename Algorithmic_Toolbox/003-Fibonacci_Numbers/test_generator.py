"""
title           :test_generator.py
description     :Automated tests for project
author          :BaffledBear (Peter Rauhut)
date            :20161009
version         :0.1
usage           :python3 test_generator.py
notes           :CTRL-C to cancel, only tests a small number of values due
notes           :to the level of inefficiency of the naive approach
python version  :3.5.2
"""
import fibonacci_numbers
import fibonacci_numbers_naive
import time

LIMIT = 30  # Naive solution escalates very quickly recommend no more than 38
HIGH_TEST = 100

if __name__ == "__main__":
    f = open('Error.log', 'w')
    print("Success")
    for test in range(0, LIMIT):
        result1 = fibonacci_numbers.fibonacci_numbers(test)
        result2 = fibonacci_numbers_naive.fibonacci_numbers(test)
        if (result1 == result2):
            print("Test OK")
        else:
            print("Test Failed: {}, {}, {}")
            f.write("Test Failed: {}, {}, {}")
    start = time.time()
    print(fibonacci_numbers.fibonacci_numbers(HIGH_TEST))
    end = time.time()
    print("Runtime: {} seconds".format(end - start))
