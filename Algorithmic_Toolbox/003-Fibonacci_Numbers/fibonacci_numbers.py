"""
title           :fibonacci_numbers.py
description     :This script solves the problem
author          :BaffledBear (Peter Rauhut)
date            :20161009
version         :0.1
usage           :python3 fibonacci_numbers.py
notes           :F(0) = 0, F(1) = 1
python version  :3.5.2
"""


def fibonacci_numbers(n):
    """
    Returns the requested number in the Fibonacci Sequence
    """
    if n <= 1:
        return n
    else:
        return fibonacci_recursive(n - 1, 1, 0)


def fibonacci_recursive(n, current, last):
    """
    Recursively calls on itself to find the requested Fibonacci number
    """
    if n < 1:
        return current
    else:
        return fibonacci_recursive(n - 1, current + last, current)
