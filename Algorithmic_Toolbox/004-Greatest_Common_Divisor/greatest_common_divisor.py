"""
title           :greatest_common_divisor.py
description     :This script solves the problem
author          :BaffledBear (Peter Rauhut)
date            :20161009
version         :0.1
usage           :python3 greatest_common_divisor.py
notes           :N/A
python version  :3.5.2
"""

def greatest_common_divisor(a, b):
    print("Start GCD")
    high = max(a, b)
    low = min(a, b)
    for i in range(1, int(low / 2) + 1):
        if low % i == 0:
            divisor = low / i
            if high % divisor == 0:
                return int(divisor)
    return 1
