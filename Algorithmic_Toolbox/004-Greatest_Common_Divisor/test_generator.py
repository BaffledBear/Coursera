"""
title           :test_generator.py
description     :Automated stress tests for project
author          :BaffledBear (Peter Rauhut)
date            :20161009
version         :0.1
usage           :python3 test_generator.py
notes           :CTRL-C to cancel
python version  :3.5.2
"""
import greatest_common_divisor as gcd
import greatest_common_divisor_sieve as gcds
import random
import time
import gc


FOR_TIME = True  # Change to True for large number test for time


def test_generator():
    """
    Generates random tests
    """
    low = 0
    high = 0
    if FOR_TIME:
        low = 10 ** 90
        high = 10 ** 100
    else:
        low = 0
        high = 10 ** 5
    test = [
        random.randint(low, high),
        random.randint(low, high)
    ]
    return test


if __name__ == "__main__":
    f = open('Error.log', 'w')
    while(True):
        test = test_generator()
        print(test)
        start = time.time()
        result1 = gcds.greatest_common_divisor(test[0], test[1])
        end = time.time()
        if not FOR_TIME:
            result2 = gcd.greatest_common_divisor(test[0], test[1])
            if result1 == result2:
                print("Test OK")
            else:
                f.write("Test Failed: {}, {}, {}".format(
                    test,
                    result1,
                    result2
                ))
        else:
            print("Runtime: {} seconds".format(end - start))
            break
        gc.collect()
