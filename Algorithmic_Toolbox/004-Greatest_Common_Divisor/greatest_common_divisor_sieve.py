"""
title           :greatest_common_divisor.py
description     :This script solves the problem
author          :BaffledBear (Peter Rauhut)
date            :20161009
version         :0.1
usage           :python3 greatest_common_divisor.py
notes           :N/A
python version  :3.5.2
"""
from functools import reduce

sieve = []
primes = []
SIEVE_ADJUSTER = 100000


def sieve_of_eratosthenes(limit):
    n = 0
    old_leng = len(sieve)
    if len(sieve) == 0:
        if limit >= SIEVE_ADJUSTER:
            n = SIEVE_ADJUSTER
        else:
            n = limit
    else:
        n = len(sieve) + SIEVE_ADJUSTER
    sieve.extend([True] * n)
    sieve[0] = False
    sieve[1] = False
    for i in range(2, n):
        if sieve[i]:
            if i > old_leng:
                primes.append(i)
            for j in range(i * 2, n, i):
                sieve[j] = False


def get_prime_factors(n):
    tracker = n
    prime_divisors = []
    for i in primes:
        while int(tracker) % int(i) == 0:
            prime_divisors.append(i)
            tracker = tracker // i
            if tracker <= 1:
                return prime_divisors
        if i >= len(sieve) - SIEVE_ADJUSTER:
            sieve_of_eratosthenes(n)
    prime_divisors.append(tracker)  # assumption that leftover is prime
    return prime_divisors


def get_common_elements(list_a, list_b):
    common = []
    for prime in list_a:
        if prime in list_b:
            common.append(prime)
            list_b.remove(prime)
    return common


def greatest_common_divisor(a, b):
    print("Start GCDS")
    high = max(a, b)
    low = min(a, b)
    sieve_of_eratosthenes(high)
    high_primes = get_prime_factors(high)
    low_primes = get_prime_factors(low)
    common_factors = get_common_elements(high_primes.copy(), low_primes.copy())
    if len(common_factors) == 0:
        return 1
    else:
        return reduce(lambda x, y: x * y, common_factors)
