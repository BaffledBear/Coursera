"""
title           :AplusB.py
description     :This script will add numbers together
author          :BaffledBear (Peter Rauhut)
date            :20161007
version         :0.1
usage           :python3 AplusB.py
notes           :Uses Python 3.5.2
python version  :3.5.2
"""


import sys


def aPlusB(numbers):
    """
    Takes in a list of numbers and returns the sum of those numbers
    """
    if len(numbers) <= 0:
        return 0
    return numbers[0] + aPlusB(numbers[1:])


def formatInput(inputStr):
    """
    Takes in a string that lists numbers separated by spaces and returns
    a list of integers.
    """
    numbers = inputStr.split(' ')
    for index, num in enumerate(numbers):
        numbers[index] = int(float(num))  # Prevents issues if float is entered
    return numbers


def validateInput(numbers):
    """
    Takes in a list of objects and verifies that each is an instance of int
    and that they are inclusively between 0 and 9.
    """
    for num in numbers:
        if not isinstance(num, int):
            return False
        elif num <= 0 or num >= 9:
            return False
    return True


if __name__ == "__main__":
    numbers = formatInput(sys.stdin.read())
    if validateInput(numbers):
        sys.stdout.write('{}\n'.format(aPlusB(numbers)))
